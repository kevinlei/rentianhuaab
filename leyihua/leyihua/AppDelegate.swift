//
//  AppDelegate.swift
//  leyihua
//
//  Created by xl12 on 2023/10/10.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var nav: NavigationMain?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white
        
        let defaults = UserDefaults.standard
         defaults.set("15013098988", forKey: String.phone + "15013098988")
        defaults.set("123456", forKey: String.password + "15013098988")
        
        defaults.synchronize()
        
        let loginSuccess: String = UserDefaults.standard.string(forKey: String.loginSuccess) ?? ""
        if loginSuccess == "success" {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController")
            nav = NavigationMain(rootViewController: vc)
            

         }else {
            nav = NavigationMain(rootViewController: LoginViewController())
             
             window?.rootViewController = UINavigationController(rootViewController: LoginViewController())

        }
        

        
        return true
    }

    

}

