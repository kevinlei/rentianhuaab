//
//  RegistViewController.swift
//  leyihua
//
//  Created by LXie on 11/10/2023.
//

import UIKit

class RegistViewController: BaseViewController {

    @IBOutlet weak var phoneField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var password2Field: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
     }


    @IBAction func sureAction(_ sender: Any) {
        let phoneNo = self.phoneField.text ?? ""
        let pwdStr = self.passwordField.text ?? ""
        let confirmPwdStr = self.password2Field.text ?? ""
        if (phoneNo.count == 0) {
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "请输入手机号", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        if (pwdStr.count == 0) {
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "请输入密码", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        //
        if pwdStr.count < 6 || pwdStr.count > 18 {
            let remind = "请设置6-18位的密码"
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: remind, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        if confirmPwdStr != pwdStr{
            let alertVC:UIAlertController = UIAlertController (title: "提示", message: "密码不一致", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "确定", style: .default, handler: nil)
            alertVC.addAction(defaultAction)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        resignPhoneAndPwd()
    }
    
    func resignPhoneAndPwd(){
 
        let defaults = UserDefaults.standard
        defaults.set(self.phoneField.text, forKey: String.phone)
        defaults.set(self.passwordField.text, forKey: String.password)
        defaults.synchronize()
        
        let alertVC:UIAlertController = UIAlertController (title: "", message: "注册成功", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "确定", style: .default) { UIAlertAction in
//            self.navigationController?.popViewController(animated: true)
//            self.dismiss(animated: true, completion: nil)
            
            self.loginSuccess()
        }
        alertVC.addAction(defaultAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func loginSuccess() {
        UserDefaults.standard.set("success", forKey: String.loginSuccess)
        UserDefaults.standard.synchronize()
        
 
        let defaults = UserDefaults.standard
        defaults.set(phoneField.text, forKey: String.phone + phoneField.text!)
        defaults.set(passwordField.text, forKey: String.password + phoneField.text!)
        
        defaults.set(phoneField.text, forKey: String.phone)

        defaults.synchronize()
        
        let window:UIWindow = (UIApplication.shared.delegate?.window)!!
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController")
 
        window.rootViewController = vc
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        
    }
    
}
